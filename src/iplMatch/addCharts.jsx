import React, { Component } from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official' ;

function AddCharts(props){
    if(props.value.showPopup==true){
        return (  
            <div className="charts" onClick={(e)=>props.handlePopUp(e)}>
                 <HighchartsReact highcharts={Highcharts} options={props.value.chartOptions}/>
               </div>
        );
    }
    else
    return null;
      
}
 
export default AddCharts;