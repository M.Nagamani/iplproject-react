import React, { Component } from 'react';
import {Link} from 'react-router-dom';
class Header extends Component {
    render() { 
        return ( 
            <div className="links">
                  <Link to="/matches_per_season" style={ {textDecoration: 'none' }}>
                    <span className="years">MatchesPerSeason</span>
                </Link>
                <Link to="/matches_won_season" style={{ textDecoration: 'none' }}>
                    <span className="years">MatchesWonPerSeason</span>
                </Link>
                <Link to="/ExtraRuns" style={{ textDecoration: 'none' }}>
                    <span className="years">ExtraRunsPerTeam</span>
                </Link>
                 <Link to="/calculations" style={{ textDecoration: 'none' }} >
                     <span className="years">IndividualScores</span>
                 </Link>
              
            </div>
         );
    }
}
 
export default Header;