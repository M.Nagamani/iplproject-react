import React, { Component } from 'react';
import AddSeasons from "./addSeasons";
class Years extends Component {
    constructor(props){
        super(props);
        this.state={
           chartOptions:{
                chart: {
                    type: 'bar',
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'yellow'],
                            [1, 'green'],
                            ]
                    },
                  },
                  xAxis: {
                      title:{
                          text:"Bowling_team"
                      },
                    categories: []
                  },
                  yAxis:{
                    title:{
                        text:"Extra_Runs"
                    } 
                  },
                  title:{
                      text:""
                  },
                series:
                    [{
                    data:[],
                }],
                plotOptions: {
                    series: {
                      point: {
                        events: {
                          mouseOver:this.setHoverData.bind(this)
                        }
                      }
                    }
                  }
                },
                hoverData: null,
                showPopup:false
              
           }
            }
    setHoverData = (e) => {
        this.setState({ hoverData: e.target.category })
      }
    handleSeasons=async (e)=>{
        let target=e.target.innerHTML
       let value=[];
       let newdata=[];
   await fetch(`http://localhost:8000/api//deliveries/extraRuns/${target}`)
     .then((res)=>res.json())
     .then((data)=>{
        data.forEach((element)=>{
      value.push(element.bowling_team);
      newdata.push(element.Extra_Runs);
       })
     })
        this.setState({
         chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
               xAxis: {
                title:{
                    text:"Bowling_team"
                },
                    categories:value
               },
               yAxis:{
                title:{
                    text:"Extra_Runs"
                } 
              },
               title:{
                   text:`Extra_Runs Per Team in -${target}`
               },
             series:
                 [{
                 name:'season',
                 data:newdata,
                     
             }],
          
         
            },
            showPopup:true
         

    })
    // console.log(this.state.chartOptions.series.map((element)=>element.data))
}
handlePopUp=(e)=>{
    this.setState({
        showPopup:false
    })
}
    render() { 
    return (
       <div>
      < AddSeasons 
      myState={this.state}
      handleClick={this.handleSeasons}
      handlePopUp={this.handlePopUp}/>
           </div>
    )
}
}
export default Years;