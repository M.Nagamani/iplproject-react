import React, { Component } from 'react';
import AddSeasons from "./addSeasons";
class TotalRuns extends Component {
 constructor(props){
     super(props)
     this.state={
        chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Bowler"
                  },
                categories: []
              },
              yAxis:{
                title:{
                    text:"Economcal_Rate"
                } 
              },
              title:{
                  text:""
              },
            series:
                [{
                    
                data:[],
            }],
        },
            showPopup:false
          
       }
    }
handlePopUp=()=>{
    this.setState({
        showPopup:false
    })
}
handleFetching=async (e)=>{
    let targetId=e.target.innerHTML;
    let bowler=[];
    let economical_Rate=[];
    await fetch(`http://localhost:8000/api/deliveries/TotalRuns/${e.target.innerHTML}`)
    .then((res)=>res.json())
    .then((data)=>{
       data.forEach((element)=>{
           bowler.push(element.bowler);
           economical_Rate.push(element.Economical_Rate);
       })
    })
    this.setState({
        chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Bowler"
                  },
                categories: bowler
              },
              yAxis:{
                title:{
                    text:"Economcal_Rate"
                } 
              },
              title:{
                  text:`Economical Rate Per Bowler -${targetId}`
              },
            series:
                [{
                  name:"economical_rate",  
                data:economical_Rate,
            }],
        },
        showPopup:true
    })
}
    render() { 
        return ( 
            <div>
                <AddSeasons
                myState={this.state}
                handlePopUp={this.handlePopUp}
                handleClick={this.handleFetching} />
            </div>
         );
    }
}
 
export default TotalRuns;