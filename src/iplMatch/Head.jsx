import React, { Component } from 'react';
import {Link} from 'react-router-dom';
class Head extends Component {
    state = {  }
    render() { 
        return (  
            <div>
                <Link to="/calculations/total_runs" style={{ textDecoration: 'none' }} >
                <span className="calculation">EconomyRatePerBowler</span>
                </Link>
                <Link to="/calculations/bowler_Avrg" style={{textDecoration:'none'}}>
                    <span className="calculation">BowlersAverage</span>
                </Link>
                <Link to="/calculations/batsman_Strike_Rate" style={{textDecoration:'none'}}>
                    <span className="calculation">BatsmanStrikeRate</span>
                </Link> 
                <Link to="/calculations/BowlerStrikeRate" style={{textDecoration:'none'}}>
                    <span className="calculation">BowlersStrikeRate</span>
                </Link> 
                <Link to="/calculations/BatsmanAverage" style={{textDecoration:'none'}}>
                    <span className="calculation">BatsmanAverage</span>
                </Link>

            </div>
        );
    }
}
 
export default Head;
