import React, { Component } from 'react';
import AddSeasons from './addSeasons';
class BatsmanAverage extends Component {
   constructor(props){
       super(props)
       this.state={
        chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Batsman"
                  },
                categories: []
              },
              yAxis:{
                title:{
                    text:"BattingStrikeRate"
                } 
              },
              title:{
                  text:""
              },
            series:
                [{
                    
                data:[],
            }],
        },
            showPopup:false
          
       }
    }
handlePopUp=()=>{
    this.setState({
        showPopup:false
    })
}
handleClick=(e)=>{
    let targetId=e.target.innerHTML;
    let batsmanAverageRate=[];
    fetch(`http://localhost:8000/api/deliveries/BattingAverage/${e.target.innerHTML}`)
    .then(res=>res.json())
    .then((data)=>{
        console.log(data);
     data.forEach((element)=>{
         if(batsmanAverageRate[element.batsman]){
            if(element.player_dismissed){
            if(element.player_dismissed==batsmanAverageRate[element.batsman]){
                batsmanAverageRate[element.batsman]["player_dismissed"]+=1;
            }             
         }
         batsmanAverageRate[element.batsman]["runs"]+=1;
        }
        else{
            batsmanAverageRate[element.batsman]={};
            if(element.player_dismissed){
                if(element.player_dismissed==element.batsman){
                    batsmanAverageRate[element.batsman]["player_dismissed"]=1;
                }
                batsmanAverageRate[element.batsman]["runs"]=element.total_runs
            }
            else{
                batsmanAverageRate[element.batsman]["player_dismissed"]=0;
                batsmanAverageRate[element.batsman]["runs"]=element.total_runs;
            }
        }
     })
     console.log(batsmanAverageRate);
    })
}
    render() { 
        return ( 
            <div>
                <AddSeasons
                myState={this.state}
                handlePopUp={this.handlePopUp}
                handleClick={this.handleClick} />

            </div>
         );
    }
}
 
export default BatsmanAverage;