import React, { Component } from 'react';
import AddCharts from "./addCharts"
function AddSeasons(props){
    return (
        <div className="season-data">
        <div className="season-years">
        <div className="season-1" onClick={(e)=>{props.handleClick(e)}}>2008</div>
        <div className="season-2" onClick={(e)=>{props.handleClick(e)}}>2009</div>
        <div className="season-3" onClick={(e)=>{props.handleClick(e)}}>2010</div>
        <div className="season-4" onClick={(e)=>{props.handleClick(e)}} >2011</div>
        <div className="season-5" onClick={(e)=>{props.handleClick(e)}}>2012</div>
        <div className="season-6" onClick={(e)=>{props.handleClick(e)}}>2013</div>
        <div className="season-7" onClick={(e)=>{props.handleClick(e)}}>2014</div>
        <div className="season-8" onClick={(e)=>{props.handleClick(e)}}>2015</div>
        <div className="season-9" onClick={(e)=>{props.handleClick(e)}}>2016</div>
        <div className="season-10" onClick={(e)=>{props.handleClick(e)}}>2017</div>
     
    </div>
    <AddCharts 
    value={props.myState}
    handlePopUp={props.handlePopUp}/>
    </div>
    )
}
export default AddSeasons;