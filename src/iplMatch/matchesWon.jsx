import React, { Component } from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official' ;
class MatchesPerSeason extends Component {
constructor(props){
    super(props)
    this.state={
        chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Seasons"
                  },
                categories: []
              },
              yAxis:{
                title:{
                    text:"Number Of Matches"
                } 
              },
              title:{
                  text:"Matches Per Season"
              },
            series:
                [{
                    name:"MatchesPerSeason",
                data:[],
            }]
        }   
        }
        }
componentDidMount=async ()=>{
    let seasons=[];
    let count=[];
    await fetch('http://localhost:8000/api/matches/season')
    .then((res)=>res.json())
    .then((data)=>{
        console.log(data)
        data.forEach((element)=>{
            seasons.push(element.season);
            count.push(element.count);
        })
    })
    this.setState({
    chartOptions:{
          xAxis: {
            categories:seasons,
          },
        series:
            [{
            data:count,
        }]
    }   
    })

}
    render() { 
        return ( 
            <div className="matches-season">
                 <HighchartsReact highcharts={Highcharts} options={this.state.chartOptions}/>
           
            </div>
         );
    }
}
 
export default MatchesPerSeason;