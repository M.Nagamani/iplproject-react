import React, { Component } from 'react';
import AddSeasons from "./addSeasons";
class BowlerStrikeRate extends Component {
   constructor(props){
       super(props)
       this.state={
 chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Batsman"
                  },
                categories: []
              },
              yAxis:{
                title:{
                    text:"BattingStrikeRate"
                } 
              },
              title:{
                  text:""
              },
            series:
                [{
                    
                data:[],
            }],
        },
            showPopup:false
          
       }
    }
handlePopUp=()=>{
    this.setState({
        showPopup:false
    })
}
handleClick=async (e)=>{
    let targetId=e.target.innerHTML;
    let bowler=[];
    let  bowlerStrikeRate2014=[]
   await fetch(`http://localhost:8000/api/deliveries/BowlingStrikeRate/${e.target.innerHTML}`)
    .then(res=>res.json())
    .then((data)=>{
       data.forEach((element)=>{
        if(bowler[element.bowler]){
                       if(element.player_dismissed){
                   bowler[element.bowler]["player_dismissed"]++;
                   }
                   bowler[element.bowler]["ball"]++;
                   }else{
                       bowler[element.bowler]={};
                       if(element.player_dismissed){
                           bowler[element.bowler]["player_dismissed"]=1;
                           bowler[element.bowler]["ball"]=1;
                       }
                       else{
                           bowler[element.bowler]["player_dismissed"]=0;
                           bowler[element.bowler]["ball"]=element.ball;
                       }
                   }
                })
    })
    for (let key in bowler) {
        let rate = (bowler[key]['ball'] / bowler[key]['player_dismissed']);
        bowlerStrikeRate2014.push([key, rate]);
    }
    bowlerStrikeRate2014.sort((a, b) => {
        return a[1] - b[1];
    });
     let bowlerStrikeRate=bowlerStrikeRate2014.slice(0, 10);
     let bowlerStrike=[];
     let playerDismissed=[];
      bowlerStrikeRate.map((element)=>{
          bowlerStrike.push(element[0]);
          playerDismissed.push(element[1]);
      })
      this.setState({
        chartOptions:{
                   chart: {
                       type: 'bar',
                       backgroundColor: {
                           linearGradient: [0, 0, 500, 500],
                           stops: [
                               [0, 'yellow'],
                               [1, 'green'],
                               ]
                       },
                     },
                     xAxis: {
                         title:{
                             text:"Bowler"
                         },
                       categories:bowlerStrike
                     },
                     yAxis:{
                       title:{
                           text:"BowlingStrikeRate"
                       } 
                     },
                     title:{
                         text:`BowlerStrikeRate per Bowler-${targetId}`
                     },
                   series:
                       [{
                           
                       data:playerDismissed,
                   }],
               },
                   showPopup:true
                 
              })
    
}


    render() { 
        return ( 
            <div>
                <AddSeasons
                myState={this.state}
                handlePopUp={this.handlePopUp}
                handleClick={this.handleClick} />

            </div>
         );
    }
}
 
export default BowlerStrikeRate;