import React, { Component } from 'react';
import AddSeasons from "./addSeasons";
class BatsmanStrikeRate extends Component {
 constructor(props){
     super(props)
     this.state={
        chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Batsman"
                  },
                categories: []
              },
              yAxis:{
                title:{
                    text:"BattingStrikeRate"
                } 
              },
              title:{
                  text:""
              },
            series:
                [{
                    
                data:[],
            }],
        },
            showPopup:false
          
       }
    }
handlePopUp=()=>{
    this.setState({
        showPopup:false
    })
}
handleFetching=async (e)=>{
    let targetId=e.target.innerHTML;
    let batsman=[];
    let strikeRate=[];
    await fetch(`http://localhost:8000/api/deliveries/BatingStrikeRate/${e.target.innerHTML}`)
    .then((res)=>res.json())
    .then((data)=>{
       data.forEach((element)=>{
           batsman.push(element.batsman);
           strikeRate.push(element.Batting_StrikeRate);
       })
    })
    this.setState({
        chartOptions:{
            chart: {
                type: 'bar',
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'yellow'],
                        [1, 'green'],
                        ]
                },
              },
              xAxis: {
                  title:{
                      text:"Batsman"
                  },
                categories: batsman
              },
              yAxis:{
                title:{
                    text:"Batting Strike Rate"
                } 
              },
              title:{
                  text:`Batting Strike Rate Per bastman -${targetId}`
              },
            series:
                [{
                  name:"Batting Strike",  
                data:strikeRate,
            }],
        },
        showPopup:true
    })
}
    render() { 
        return ( 
            <div>
                
                <AddSeasons
                myState={this.state}
                handlePopUp={this.handlePopUp}
                handleClick={this.handleFetching} />
            </div>
         );
    }
}
 
export default BatsmanStrikeRate;