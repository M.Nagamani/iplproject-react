import React, { Component } from 'react';
import AddSeasons from "./addSeasons"
class BowlersAvrge extends Component {
    constructor(props){
        super(props)
        this.state={
            chartOptions:{
                chart: {
                    type: 'bar',
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'yellow'],
                            [1, 'green'],
                            ]
                    },
                  },
                  xAxis: {
                      title:{
                          text:"Bowler"
                      },
                    categories: []
                  },
                  yAxis:{
                    title:{
                        text:"Economcal_Rate"
                    } 
                  },
                  title:{
                      text:""
                  },
                series:
                    [{
                        
                    data:[],
                }],
            },
                showPopup:false
        }
    }
    handleClick=async (e)=>{
       let targetId=e.target.innerHTML;
       let bowlers=[];
       let averageRate=[];
       await fetch(`http://localhost:8000/api/deliveries/BowlerAverage/${e.target.innerHTML}`)
        .then((res)=>res.json())
        .then((data)=>{
            console.log(data);
          data.forEach((element)=>{
              bowlers.push(element.bowler);
              averageRate.push(element.Bowler_Average)
        })
        })
        this.setState({
                chartOptions:{
                    chart: {
                        type: 'bar',
                        backgroundColor: {
                            linearGradient: [0, 0, 500, 500],
                            stops: [
                                [0, 'yellow'],
                                [1, 'green'],
                                [2,"blue"]
                                ]
                        },
                      },
                      xAxis: {
                          title:{
                              text:"Bowler"
                          },
                        categories: bowlers
                      },
                      yAxis:{
                        title:{
                            text:"Bowlers_Average"
                        } 
                      },
                      title:{
                          text:`Bowlers_Average Per Bowler -${targetId}`
                      },
                    series:
                        [{
                          name:"Bowlers_Average",  
                        data:averageRate,
                    }],
                },
                showPopup:true
    
        })
    }
    handlePopUp=(e)=>{
        this.setState({
            showPopup:false
        })
    }
    render() { 
        return (
            <div>
                <AddSeasons 
                myState={this.state}
                handlePopUp={this.handlePopUp}
                handleClick={this.handleClick}/>
            </div>
          );
    }
}
 
export default BowlersAvrge;