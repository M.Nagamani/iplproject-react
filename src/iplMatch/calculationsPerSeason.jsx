import React, { Component } from 'react';
import Head from './Head';
import TotalRuns from "./TotalRuns";
import BowlersAvrge from "./bowlersAvrge";
import BatsmanStrikeRate from "./batsmanStrikeRate";
import BowlerStrikeRate from "./bowlerStrikeRate";
import BatsmanAverage from "./batsmanAverage";
import { BrowserRouter as Router, Route } from "react-router-dom";
class Calculations extends Component {
    render() { 
        return ( 
            <Router>
            <div>
              <Head />
              <Route path="/calculations/total_runs"   component={TotalRuns}/>
              <Route path="/calculations/bowler_Avrg" component={BowlersAvrge}/>
              <Route path="/calculations/batsman_Strike_Rate" component={BatsmanStrikeRate} />
              <Route path="/calculations/BowlerStrikeRate" component={BowlerStrikeRate} />
              <Route path="/calculations/BatsmanAverage" component={BatsmanAverage} />
            </div>
            </Router>
         );
    }
}
 
export default Calculations;