import React, { Component } from 'react';
import AddSeasons from "./addSeasons";

class MatchesWonPerSeason extends Component {
    constructor(props){
        super(props)
        this.state={
            chartOptions:{
                chart: {
                    type: 'bar',
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'yellow'],
                            [1, 'green'],
                            ]
                    },
                  },
                  xAxis: {
                      title:{
                          text:"Bowling_team"
                      },
                    categories: []
                  },
                  yAxis:{
                    title:{
                        text:"Extra_Runs"
                    } 
                  },
                  title:{
                      text:""
                  },
                series:
                    [{
                        
                    data:[],
                }],
            },
                showPopup:false
              
           }
        }
    
    handleFetching=async (e)=>{
        let target=e.target.innerHTML;
        let matchesTeam=[];
        let matchesWonTimes=[];
       await fetch(`http://localhost:8000/api/matches/won/${e.target.innerHTML}`)
        .then(res=>res.json())
        .then((data)=>{
         data.forEach((element)=>{
             matchesTeam.push(element.winner);
             matchesWonTimes.push(element.count);
            })
        })
        this.setState({
            chartOptions:{
                chart: {
                    type: 'bar',
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'yellow'],
                            [1, 'green'],
                            ]
                    },
                  },
                  xAxis: {
                      title:{
                          text:"Winner-Team"
                      },
                    categories: matchesTeam
                  },
                  yAxis:{
                    title:{
                        text:"Number of times won match"
                    } 
                  },
                  title:{
                      text:`Matches Won Per Team per Season -${target}`
                  },
                series:
                    [{
                        name:"Matches won teams per season",
                    data:matchesWonTimes,
                }],
            },
            showPopup:true
        })
    }
    handlePopUp=()=>{
        this.setState({
            showPopup:false
        })
    }
    render() { 
        return (
            <div>
                <AddSeasons 
                 myState={this.state} 
                 handleClick={this.handleFetching}
                 handlePopUp={this.handlePopUp}/>
            </div>
          );
    }
}
 
export default MatchesWonPerSeason;