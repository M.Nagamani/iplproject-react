import React, { Component } from 'react';
import Header from './content';
import MatchesPerSeason from './matchesWon'
import Years from './seasons';
import MatchesWonPerSeason from "./matchesWonPerSeason";
import Calculations from './calculationsPerSeason';
import { BrowserRouter as Router, Route } from "react-router-dom";
class IplMatch extends Component {
    render() { 
        return (
            <Router>
            <div>
                <div className="header">
                <span className="main-header">IplMatch</span>
                </div>
                <Header />
                <Route path="/matches_per_season" component={MatchesPerSeason} />
               <Route path="/matches_won_season"  component={MatchesWonPerSeason} />
               <Route path="/ExtraRuns" component={Years} />
                 <Route path="/calculations"  component={Calculations} />
            </div>
            </Router> 
         );
    }
}
 
export default IplMatch;