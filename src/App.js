import React from 'react';
import IplMatch from "./iplMatch/matches"

function App() {
  return (
    <div className="App">
      <IplMatch />
    </div>
  );
}

export default App;
